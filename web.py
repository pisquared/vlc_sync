from flask import Flask, jsonify, request
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import check_password_hash

from common import Observer
from sensitive_pwd import users

app = Flask("VlcProxyServer")

auth = HTTPBasicAuth()

ROOMS = {

}


class Room(object):
    def __init__(self, room_name):
        self.room_name = room_name
        self.status = dict()

        self.observers = []

    @staticmethod
    def get_or_create(room_name):
        room = ROOMS.get(room_name)
        if not room:
            room = Room(room_name=room_name)
            ROOMS[room_name] = room
        return room

    def register_observer(self, observer):
        self.observers.append(observer)

    def maybe_update(self, status):
        old_idx, new_idx = self.status.get('idx', 0), status.get('idx')
        if new_idx > old_idx:
            self.status = status
            for observer in self.observers:
                observer.notify()
            self.observers = []
            return True
        return False

    def serialize(self):
        return {
            "room_name": self.room_name,
            "status": self.status,
        }


@auth.verify_password
def verify_password(username, password):
    if username in users:
        return check_password_hash(users.get(username), password)
    return False


@app.route('/')
@auth.login_required
def index():
    return jsonify({"auth": auth.username()})


@app.route('/rooms/<room_name>/status', methods=["GET", "POST"])
@auth.login_required
def room_register(room_name):
    room = Room.get_or_create(room_name)
    new_idx = int(request.form.get("idx", 0))
    new_state = request.form.get("state")
    new_time = request.form.get("time")
    new_status = {'idx': new_idx, 'state': new_state, 'time': new_time}
    room.maybe_update(new_status)
    return jsonify(room.serialize())


@app.route('/rooms/<room_name>/long')
@auth.login_required
def long_poll(room_name):
    room = Room.get_or_create(room_name)
    observer = Observer()
    room.register_observer(observer)
    observer.wait()
    return jsonify(room.serialize())


if __name__ == '__main__':
    app.run(host="0.0.0.0", threaded=True)
