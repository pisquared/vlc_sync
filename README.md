# VLC Syncer
Syncs play/pause states of vlc instances.

The instances can be in two separate locations, synced by a central server.

The instances are assumed to have the same videofile.

Run the client which will start a local instance of vlc and a syncer process that monitors the central server for changes.

## Install
```bash
virtualenv -p python3 venv
pip install -r requirements.txt
source venv/bin/activate
```

## Run Client
Make sure that file `sensitive.py` has something like:

```python
s_config = dict(
    user="<username>",
    password="<password>",
)
```

then
```bash
source venv/bin/activate
python vlc.py
```

## Run Server
Make sure that file `sensitive_pwd.py` has something like:

```python
from werkzeug.security import generate_password_hash

users = {
    "<user>": generate_password_hash("<password>"),
}
```

then
```bash
source venv/bin/activate
python web.py
```