from threading import Event

from datetime import datetime


def get_utcnow():
    return datetime.utcnow()


def print_console(proc_type, message):
    now = get_utcnow()
    print("{} [{}]: {}".format(now, proc_type, message))


class Observer(object):
    def __init__(self):
        self.flag = Event()

    def notify(self):
        self.flag.set()

    def wait(self):
        self.flag.wait()

    def reset(self):
        self.flag.clear()
