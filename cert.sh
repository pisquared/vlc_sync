#!/usr/bin/env bash

PROJECT_DOMAIN="sync.pi2.dev"

#[ ! -f /tmp/certbot-auto ] && wget -O /tmp/certbot-auto https://dl.eff.org/certbot-auto
#chmod +x /tmp/certbot-auto
#certbot=/tmp/certbot-auto
sudo apt install certbot
certbot certonly --authenticator standalone --installer nginx --pre-hook "service nginx stop" --post-hook "service nginx start" --redirect --agree-tos --no-eff-email --email admin@app.com -d ${PROJECT_DOMAIN} --no-bootstrap