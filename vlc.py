import argparse
import requests
import subprocess
import threading
from requests.auth import HTTPBasicAuth
from time import sleep

import sensitive as def_s_config
import user_config as def_config
from common import print_console

PROC_VLC_CLIENT = "VLC_CLIENT"
METHOD_POST, METHOD_GET = "post", "get"


def execute_shell_command(command):
    output = subprocess.check_output(command, shell=True)
    return output


class Threaded(object):
    def __init__(self, thread_name):
        self.thread_name = thread_name
        self.thread = None

    def start(self):
        print_console(self.thread_name, "starting thread")
        if not self.thread:
            self.thread = threading.Thread(target=self._start_thread, args=(), )
            self.thread.start()
        print_console(self.thread_name, "started thread")

    def stop(self):
        print_console(self.thread_name, "stopping thread")
        if self.thread:
            self._stop_thread()
            self.thread.join()
        print_console(self.thread_name, "stopped thread")

    def _start_thread(self):
        raise NotImplementedError

    def _stop_thread(self):
        raise NotImplementedError


class VlcLocalRC(Threaded):
    def __init__(self, host, port):
        super().__init__("VlcLocalRC")
        self.host = host
        self.port = port

        self.process = None

    def _start_thread(self):
        command = "vlc --intf qt --extraintf rc --rc-host {host}:{port}".format(host=self.host, port=self.port)

        with subprocess.Popen(command.split(), stdout=subprocess.PIPE,
                              stderr=subprocess.STDOUT,
                              bufsize=1, universal_newlines=True) as p:
            self.process = p
            for line in self.process.stdout:
                print_console(self.thread_name, message=line)

        if self.process.returncode != 0:
            print_console(self.thread_name,
                          message="EXIT with {} - {}".format(p.returncode,
                                                             p.args))

    def _stop_thread(self):
        if self.process:
            self.process.terminate()

    def send_vlc_command(self, command):
        # print_console(self.thread_name, "sending {}".format(command))
        output = execute_shell_command(
            "echo '{command}' | netcat -N {host} {port}".format(command=command,
                                                                host=self.host,
                                                                port=self.port, ))
        # print_console(PROC_VLC_CLIENT, message=output)
        clean_output = ""
        for line in output.decode("utf-8").split('\n')[2:]:
            if "Bye-bye!" in line:
                continue
            elif line.startswith(">"):
                line = '\n'.join(line.split(">")[1:])
                clean_output += line.strip()
            else:
                clean_output += line.strip() + '\n'
        # print_console(self.thread_name, "received: {}".format(clean_output))
        return clean_output

    def get_status(self):
        s_time = self.send_vlc_command("get_time").strip() or '0'
        all_status = self.send_vlc_command("status")
        for line in all_status.split('\n'):
            if "state" in line:
                s_state = line.split("state")[1].split(')')[0].strip()
                break
        else:
            s_state = "unknown"
        return {"time": s_time, "state": s_state}


class VlcProxyClient(object):
    """Provides a connection to a remote instance"""

    def __init__(self, remote_host, room_name, auth):
        self.thread_name = "VlcProxyClient"
        self.remote_host = remote_host
        self.room_name = room_name
        self.auth = auth

    def do_request(self, method, path, data=None):
        if method == METHOD_POST:
            resp = requests.post("{}{}".format(self.remote_host, path), data=data,
                                 auth=HTTPBasicAuth(*self.auth))
        elif method == METHOD_GET:
            resp = requests.get("{}{}".format(self.remote_host, path),
                                auth=HTTPBasicAuth(*self.auth))
        else:
            resp = None
        if not resp or resp.status_code != 200:
            print_console(self.thread_name, "Problem sending update".format())
            rv = {"error": "Problem sending update"}
        else:
            rv = resp.json()
        return rv

    def send_update(self, status):
        print_console(self.thread_name, "sending update: {}".format(status))
        rv = self.do_request(METHOD_POST, "/rooms/{}/status".format(self.room_name), data=status)
        print_console(self.thread_name, "sent update: {} <- {}".format(rv, status))
        return rv

    def wait_update(self):
        print_console(self.thread_name, "waiting on update".format())
        rv = self.do_request(METHOD_GET, "/rooms/{}/long".format(self.room_name))
        print_console(self.thread_name, "received update: {}".format(rv))
        return rv


class LocalStatus(object):
    def __init__(self):
        self.s_idx = 0
        self.s_time = None
        self.s_state = None

    def serialize(self):
        return {
            "idx": self.s_idx,
            "time": self.s_time,
            "state": self.s_state,
        }

    def maybe_update(self, new_dict_status):
        new_idx = new_dict_status.get("idx", 0) or 0
        new_time = new_dict_status.get("time")
        new_state = new_dict_status.get("state")
        if new_idx > self.s_idx:
            self.s_idx = new_idx
            if new_state != self.s_state:
                self.s_state = new_state
                if new_time != self.s_time:
                    self.s_time = new_time
            return True
        return False

    def update(self, new_dict_status):
        new_time = new_dict_status.get("time")
        new_state = new_dict_status.get("state")
        if new_state != self.s_state:
            self.s_idx += 1
            self.s_state = new_state
            if new_time != self.s_time:
                self.s_time = new_time
            return True
        return False


class SyncerRemote(Threaded):
    def __init__(self, local_status, vlc_local_rc, vlc_proxy_client, config):
        super().__init__("SyncerRemote")
        self.vlc_local_rc = vlc_local_rc
        self.vlc_proxy_client = vlc_proxy_client
        self.local_status = local_status

        self.user = config.get('user')

    def _process_new_status(self, new_status):
        return self.local_status.maybe_update(new_status)

    def _start_thread(self):
        while True:
            new_status = self.vlc_proxy_client.wait_update()
            print_console(self.thread_name, new_status)
            if self._process_new_status(new_status.get('status', {})):
                self.sync()

    def sync(self):
        print_console(self.thread_name, "Syncing...")
        state = self.local_status.s_state
        time = self.local_status.s_time
        if state in ['paused']:
            self.vlc_local_rc.send_vlc_command('pause')
        elif state in ['playing']:
            if time is not None:
                self.vlc_local_rc.send_vlc_command('seek {}'.format(time))
            self.vlc_local_rc.send_vlc_command('play')

    def _stop_thread(self):
        pass


class SyncerLocal(Threaded):
    def __init__(self, local_status, vlc_local_rc, vlc_proxy_client, config):
        super().__init__("SyncerLocal")
        self.vlc_local_rc = vlc_local_rc
        self.vlc_proxy_client = vlc_proxy_client
        self.local_status = local_status

        self.push_ms = config.get('push_ms')

    def _start_thread(self):
        while True:
            local_status_dict = self.vlc_local_rc.get_status()
            is_updated = self.local_status.update(local_status_dict)
            if is_updated:
                self.vlc_proxy_client.send_update(self.local_status.serialize())
            sleep(self.push_ms / 1000)

    def _stop_thread(self):
        pass


class Syncer(object):
    def __init__(self, config):
        local_status = LocalStatus()

        self.user = config.get('user')
        self.password = config.get('password')
        self.sync_server = config.get('sync_server')
        self.local_vlc_port = config.get('local_vlc_port')
        self.push_ms = config.get('push_ms')
        self.room_name = config.get('room_name')

        self.vlc_local_rc = VlcLocalRC("localhost", self.local_vlc_port)
        self.vlc_proxy_client = VlcProxyClient(self.sync_server,
                                               auth=(self.user, self.password),
                                               room_name=self.room_name)
        self.vlc_local_rc.start()
        sleep(0.2)
        self.syncer_local = SyncerLocal(local_status,
                                        vlc_local_rc=self.vlc_local_rc,
                                        vlc_proxy_client=self.vlc_proxy_client,
                                        config=config)
        self.syncer_remote = SyncerRemote(local_status,
                                          vlc_local_rc=self.vlc_local_rc,
                                          vlc_proxy_client=self.vlc_proxy_client,
                                          config=config)

    def start(self):
        self.syncer_local.start()
        self.syncer_remote.start()


def get_config():
    parser = argparse.ArgumentParser()
    parser.add_argument('--user', dest='user')
    parser.add_argument('--password', dest='password')
    parser.add_argument('--local_vlc_port', dest='local_vlc_port')
    args = parser.parse_args()

    config = def_config.config
    config.update(def_s_config.s_config)
    update_args = vars(args)
    rv = {}
    for k, v in config.items():
        if update_args.get(k) is not None:
            rv[k] = update_args[k]
        else:
            rv[k] = v
    return rv


def main():
    config = get_config()
    syncer = Syncer(config)
    syncer.start()


if __name__ == "__main__":
    main()
